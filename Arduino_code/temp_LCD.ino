#include <DallasTemperature.h>
#include <OneWire.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

// DATA WIRE IS PLUGGED INTO PORT 2 ON THE ARDUINO
#define ONE_WIRE_BUS 7
//SETUP A oneWire INSTANCE TO COMMUNICATE WITH ANY OneWire DEVICES
OneWire oneWire(ONE_WIRE_BUS);
//PASS OUR OneWire references to Dallas Temperature
DallasTemperature sensors(&oneWire);
//arrays to hold device address
DeviceAddress insideThermometer;

LiquidCrystal_I2C lcd(0x27,16,2);

#define delays 300000

int n=5; //number of repetitions in the measurement 
float tempC = 0;
unsigned long oldMillis = 0;


void setup()
{
  lcd.init();
  lcd.clear();
  //start serial port
  Serial.begin(115200);

  sensors.begin();

  printAll();
  

  
  //Method 1
  //search for devices on the bus and assing based on an index, ideally
  //you would do this to initially discover address on the bus and then
  //use those addresses and manually assing then (see above) once you know
  //the devices on your bus (and assuaning they dont change)
  if(!sensors.getAddress(insideThermometer, 0)) Serial.println("Unable to find address for 0");

   // set the resolution to 9 bit
  //sensors.setResolution(insideThermometer, 16);
  //delay(1000);
  //lcd.clear();
}

void loop()
{

  
  if(millis() >= oldMillis + delays){
        oldMillis += delays;
        printAll();//use a simple function to print out the dat
        lcd.setCursor(0,0);
   }
 

  
}

//function to print the temperature for a device
void printAll()
{
  //lcd.clear();
  sensors.requestTemperatures();
  
  for (int i=1; i<=n; i++){
      tempC= tempC+sensors.getTempCByIndex(0);
      //delay(100);
    };
    
  tempC = tempC/n;
  
  Serial.print(" ");
  Serial.println(tempC);

  lcd.print("Temp C: ");
  lcd.print(tempC);
  lcd.setCursor(0,1);
  tempC = 0;
    
  
}
