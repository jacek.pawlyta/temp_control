from datetime import datetime, timezone
import pytz
import matplotlib.pyplot as plt
import pandas as pd
import glob
import locale


def main():
    locale.setlocale(locale.LC_ALL, 'C')
    # get name of the latest file
    path = '/home/jacek/temperature/'
    my_files = glob.glob(path+'temp*.txt')
    my_files.sort()
    last_file_name = my_files[len(my_files)-1]

    # read data from the last file in the folder, get rid of failed measurements
    temp = pd.read_csv(last_file_name, sep='\t', names=['date', 'temperature'])
    temp_data = pd.DataFrame(temp[temp.temperature != -127.00])
    tmp_data = []

    # convert datetime string to datetime object and convert time from UTC to local time zone
    for daytime in temp_data['date']:
        my_time = datetime.strptime(daytime, '%Y/%b/%d %H:%M:%S')
        my_time = my_time.replace(tzinfo=timezone.utc)
        my_time = my_time.astimezone(pytz.timezone('Europe/Warsaw'))
        tmp_data.append(my_time)

    temp_data.date = tmp_data

    plt.plot_date(temp_data['date'].tail(500),
                  temp_data['temperature'].tail(500),
                  fmt="-", xdate=True, ydate=False)
    plt.xlabel('Time of the measurement')
    plt.gcf().autofmt_xdate()
    plt.ylabel('Temperature (°C)')
    plt.title('Last measured: '+temp_data.tail(1).to_string(header=False, index=False)+'°C')
    plt.savefig('static/temperature_plot.svg')
#    plt.show()
    plt.close()
