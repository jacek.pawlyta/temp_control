from flask import Flask, render_template, url_for
from main import main

app = Flask(__name__)
app.secret_key = b'\xe5\xa2\x1b\xec\xc8\xf5\x94\x87\x84\xf85Vd\xcfRR'


@app.route('/', methods=['GET', 'POST'])
def index_file():
    main()
    return render_template('index_file.html')


if __name__ == '__main__':
    app.debug = True
    app.run(port=5050, host='0.0.0.0')
