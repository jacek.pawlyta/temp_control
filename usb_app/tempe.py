#!/bin/python3
import serial
from time import *

PATH = '/home/jacek/temperature/'

ser = serial.Serial('/dev/ttyACM0',
                    baudrate=115200,
                    bytesize=serial.EIGHTBITS,
                    parity=serial.PARITY_NONE,
                    stopbits=serial.STOPBITS_ONE,
                    timeout=99000,
                    xonxoff=False,
                    rtscts=False,
                    writeTimeout=20,
                    dsrdtr=False)
# l=[]
# name of the file with data
datownik = strftime('%Y-%b-%d_%H_%M_%S', gmtime())
fname = 'temp_' + datownik + '.txt'


f = open(PATH+fname, 'a')
f.close()

while True:
    temperatura = str(ser.readline(), 'latin1')
    datownik = strftime('%Y/%b/%d %H:%M:%S', gmtime())
    linia = datownik + '\t' + temperatura
    print(linia, end='')
    f = open(PATH+fname, 'a')
    f.write(linia)
    f.close()

print('The End')
ser.close()
